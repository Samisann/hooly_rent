<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Security\CustomAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class RegistrationController extends AbstractController
{
    #[Route('/register', name: 'app_register')]
    public function register(Request $request, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager): JsonResponse
    {
        $response = new JsonResponse();
        
    
        $data = json_decode($request->getContent(), true);
    
        if (isset($data['email']) && isset($data['password'])) {
            $user = new User();
            $user->setEmail($data['email']);
            $user->setNom($data['nom']);
            $user->setPrenom($data['prenom']);
            $user->setAnniversaire(new \DateTime($data['anniversaire']));
            $user->setTelephone($data['telephone']);

            $user->setInscription(new \DateTime());
            $user->setRoles(['ROLE_USER']);
            $user->setPassword(password_hash($data['password'], PASSWORD_DEFAULT));
            $entityManager->persist($user);
            dump($user);
            $entityManager->flush();
            
            $response->setData([
                'status' => 'success',
                'message' => 'User created successfully',
           
            ]);
        } else {
            $response->setData([
                'status' => 'error',
                'message' => 'Invalid form data'
            ]);
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
    
        return $response;
    }
    
}
